package fr.univlille.iutinfo.m3105.ex1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class FooBarQixTest {
	private static final String FOO = "Foo";
	private static final String BAR = "Bar";
	private static final String QIX = "Qix";
	private static final String EMPTY = "";
	
	@Test
	protected void modulo_foo() {
		assertEquals(FOO, fooBarQix(6));
		assertEquals(FOO, fooBarQix(9));
		assertEquals(FOO, fooBarQix(12));
	}
	
	@Test
	protected void modulo_bar() {
		assertEquals(BAR, fooBarQix(10));
		assertEquals(BAR, fooBarQix(20));
		assertEquals(BAR, fooBarQix(40));
	}
	
	@Test
	protected void modulo_qix() {
		assertEquals(QIX, fooBarQix(14));
		assertEquals(QIX, fooBarQix(28));
	}
	
	@Test
	protected void modulo_composite() {
		assertEquals(FOO + QIX, fooBarQix(21));
		assertEquals(FOO + BAR + BAR, fooBarQix(15));
	}
	
	@Test
	protected void should_return_foo_when_contains_three() {
		assertEquals(FOO, fooBarQix(13));
		assertEquals(FOO, fooBarQix(32));
		assertEquals(FOO + FOO, fooBarQix(331));
		assertEquals(FOO + FOO, fooBarQix(332));
	}
	
	@Test
	protected void should_return_bar_when_contains_five() {
		assertEquals(BAR, fooBarQix(512));
		assertEquals(BAR, fooBarQix(52));
		assertEquals(BAR + BAR, fooBarQix(554));
		assertEquals(BAR + BAR, fooBarQix(556));
	}
	
	@Test
	protected void should_return_qix_when_contains_seven() {
		assertEquals(QIX, fooBarQix(17));
		assertEquals(QIX + QIX, fooBarQix(776));
		assertEquals(QIX + QIX, fooBarQix(778));
	}
	
	@Test
	protected void contains() {
		assertEquals(FOO + QIX, fooBarQix(37));
		assertEquals(QIX + FOO, fooBarQix(73));
		assertEquals(QIX + FOO + FOO + QIX, fooBarQix(7337));
		assertEquals(QIX + FOO + FOO + QIX, fooBarQix(171313171));
		assertEquals(BAR + FOO, fooBarQix(53));
		assertEquals(BAR + FOO, fooBarQix(523));
		assertEquals(BAR + FOO + FOO, fooBarQix(533));
		assertEquals(BAR + FOO + FOO, fooBarQix(5633));
	}
	
	@Test
	protected void modulo_and_contains() {
		assertEquals(FOO + FOO, fooBarQix(3));
		assertEquals(BAR + BAR, fooBarQix(5));
		assertEquals(QIX + QIX, fooBarQix(7));
		
		assertEquals(FOO + BAR + BAR, fooBarQix(15));
		assertEquals(BAR + BAR + QIX + BAR, fooBarQix(575));
	}
	
	@Test
	protected void returns_n_tostring() {
		assertEquals("1", fooBarQix(1));
		assertEquals("2", fooBarQix(2));
		assertEquals("4", fooBarQix(4));
		assertEquals("8", fooBarQix(8));
	}
	
	@Test
	protected void special_case_zero() {
		assertEquals(EMPTY, fooBarQix(0));
	}
	
	private String fooBarQix(int n) {
		return FooBarQix.fooBarQix(n);
	}
}
