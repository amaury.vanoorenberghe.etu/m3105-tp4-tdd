package fr.univlille.iutinfo.m3105.ex2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class AdditionatorTest {
	public static final String ERR_EXPECTED_NUMBER_AT_END_OF_STRING = Errors.EXPECTED_NUMBER_AT_END_OF_STRING.get();
	
	@Test
	protected void returns_corrent_sum() {
		assertEquals("3.0", additionator("1.0,0.5,1.5"));
		assertEquals("4.0", additionator("1.0,2.0,1.0"));
		assertEquals("8.0", additionator("5.25,2.75"));
	}
	
	@Test
	protected void error_when_ends_with_comma() {
		assertEquals(ERR_EXPECTED_NUMBER_AT_END_OF_STRING, additionator("1.0,2.0,3.0,"));
		assertEquals(ERR_EXPECTED_NUMBER_AT_END_OF_STRING, additionator(","));
	}
	
	@Test
	protected void returns_zero_when_empty_input() {
		assertEquals("0.0", additionator(""));
	}
	
	@Test
	protected void newline_is_valid_separator() {
		assertEquals("3.0", additionator("1.0\n0.5\n1.5"));
		assertEquals("4.0", additionator("1.0\n2.0\n1.0"));
		assertEquals("8.0", additionator("5.25\n2.75"));
	}
	
	@Test
	protected void mixed_separator() {
		assertEquals("3.0", additionator("1.0,0.5\n1.5"));
		assertEquals("4.0", additionator("1.0\n2.0,1.0"));
		assertEquals("8.0", additionator("5.25\n2.75,0.0"));
	}
	
	private String additionator(String s) {
		return Additionator.additionator(s);
	}
}
