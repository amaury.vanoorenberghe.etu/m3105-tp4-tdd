package fr.univlille.iutinfo.m3105.ex1;

public class FooBarQixRule {
	private final int VALUE;
	private final String TEXT;
	
	public FooBarQixRule(int value, String text) {
		VALUE = value;
		TEXT = text;
	}
	
	public int getValue() {
		return VALUE;
	}
	
	public String getText() {
		return TEXT;
	}
	
	@Override
	public String toString() {
		return String.format("%d => %s", VALUE, TEXT);
	}
}