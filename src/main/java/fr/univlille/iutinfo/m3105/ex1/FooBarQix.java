package fr.univlille.iutinfo.m3105.ex1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FooBarQix {
	private static final List<FooBarQixRule> rules = new ArrayList<FooBarQixRule>();
	
	static {
		addRule(3, "Foo");
		addRule(5, "Bar");
		addRule(7, "Qix");
	}
	
	public static String fooBarQix(int n) {
		StringBuilder sb = new StringBuilder();
		
		moduloRules(sb, n);
		containsRules(sb, n);
		toStringRule(sb, n);
		
		return sb.toString();
		
		// TODO
		//if (moduloAndContainNotApplicable(n)) return toStringRules(n);
		//return moduloRules( n) + containsRules(n);
	}
	
	protected static void moduloRules(StringBuilder sb, int n) {
		for (FooBarQixRule rule : rules) {
			moduloRule(sb, n, rule.getValue(), rule.getText());
		}
	}
	
	protected static void moduloRule(StringBuilder sb, int n, int mod, String text) {
		if (n != 0 && n % mod == 0) {
			sb.append(text);
		}
	}
	
	protected static void containsRules(StringBuilder sb, int n) {
		String number = Integer.toString(n);
		
		for (int i = 0; i < number.length(); ++i) {
			char c = number.charAt(i);
			
			for (FooBarQixRule rule : rules) {
				containsRule(sb, c, rule.getValue(), rule.getText());
			}
		}
	}

	protected static void containsRule(StringBuilder sb, char n, int c, String text) {
		final char chr = Integer.toString(c).charAt(0);
		
		if (chr == n) {
			sb.append(text);
		}
	}
	
	protected static void toStringRule(StringBuilder sb, int n) {
		if (sb.length() == 0 && n != 0) {
			sb.append(Integer.toString(n));
		}
	}
	
	protected static void addRule(int value, String text) {
		rules.add(new FooBarQixRule(value, text));
	}
}
