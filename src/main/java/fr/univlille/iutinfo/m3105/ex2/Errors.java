package fr.univlille.iutinfo.m3105.ex2;

public enum Errors {
	EXPECTED_NUMBER_AT_END_OF_STRING("Nombre attendu en fin de chaîne");
	
	private final String FORMAT;
	
	private Errors(String format) {
		FORMAT = format;
	}
	
	public String get(Object... args) {
		return String.format(FORMAT, args);
	}
}
