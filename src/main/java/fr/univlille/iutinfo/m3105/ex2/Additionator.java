package fr.univlille.iutinfo.m3105.ex2;

import java.util.Arrays;

public class Additionator {
	public static final String SEPARATOR_REGEX = ",|\\n";
	
	public static String additionator(String s) {
		double result = 0;
		
		// Renvoie 0 si la chaine est vide
		if (s.length() == 0) {
			return "0.0";
		}
		
		// Renvoie une erreur si la chaine se termine par une virgule
		if (s.endsWith(",")) {
			return Errors.EXPECTED_NUMBER_AT_END_OF_STRING.get();
		}
		
		String[] values = s.split(SEPARATOR_REGEX);
		
		boolean hasEmptyValue = Arrays
		.stream(values)
		.anyMatch(x -> x.length() == 0);
		
		if (hasEmptyValue) {
			
		}
		
		//System.out.println(String.format("%s -> %s", s, Arrays.toString(values)));
		
		result = Arrays
		.stream(values)
		.mapToDouble(x -> Double.parseDouble(x))
		.sum();
		
		return Double.toString(result);
	}
}
